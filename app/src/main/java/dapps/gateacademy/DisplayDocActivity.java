package dapps.gateacademy;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.support.v4.net.ConnectivityManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.prefs.Preferences;

public class DisplayDocActivity extends AppCompatActivity {

    WebView mWebView;
    String pageurl="https://docs.google.com/document/d/13Vk4a-ok0U8ZjFavmlYpxWM14Q2s-J38ZR5KStGIPO0/edit?pref=2&pli=1#heading=h.qvuhw9xcl3j4";
    Button backButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_doc);
        mWebView=(WebView)findViewById(R.id.webview);
        mWebView.getSettings().setJavaScriptEnabled(false);
        mWebView.getSettings().setAppCacheMaxSize(1024*1024*5);
        mWebView.getSettings().setDisplayZoomControls(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.getSettings().setAppCachePath(getApplicationContext().getCacheDir().getAbsolutePath());
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);

        final ProgressDialog mProgressDialog=new ProgressDialog(this);
        mProgressDialog.setMessage("few seconds...");
        WebViewClient mWebViewClient=new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mProgressDialog.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgressDialog.hide();
            }
        };

        backButton=(Button)findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mWebView.setWebViewClient(mWebViewClient);
        if(checkNetwork()){
            mWebView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        }
        mWebView.loadUrl(pageurl);

        new DLpage().execute();
        /*
        HttpClient httpClient=new DefaultHttpClient();*/


    }

    private boolean checkNetwork(){
        ConnectivityManager connectivityManagerCompat=(ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        return connectivityManagerCompat.getActiveNetworkInfo()!=null && connectivityManagerCompat.getActiveNetworkInfo().isConnected();
    }
}
class DLpage extends AsyncTask{

    String html="hhkkjj";
    String pageurl="https://docs.google.com/document/d/13Vk4a-ok0U8ZjFavmlYpxWM14Q2s-J38ZR5KStGIPO0/edit?pref=2&pli=1#heading=h.qvuhw9xcl3j4";

    @Override
    protected Object doInBackground(Object[] params) {


        try {
            URL url=new URL(pageurl);
            HttpURLConnection httpURLConnection=(HttpURLConnection)url.openConnection();
            InputStream inputStream=new BufferedInputStream(httpURLConnection.getInputStream());
            BufferedReader br=new BufferedReader(new InputStreamReader(inputStream));
            String line="";
            while ((line=br.readLine())!=null){
                System.out.println(line);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i("HTMLCONTENT","got");
/*
        System.out.println(html);*/
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        Log.i("HTMLCONTENT","done");
        System.out.println(html);
    }
}
